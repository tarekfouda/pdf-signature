part of 'pdf_viewer_cubit.dart';

class PdfViewState extends Equatable {
  final File? pdfDocument;
  final Rect? rect;
  final int? currentPage;
  final Size? pageSize;
  final int? totalPage;

  const PdfViewState({
    this.pdfDocument,
    this.rect,
    this.currentPage,
    this.pageSize,
    this.totalPage,
  });

  @override
  List<Object?> get props => [
        pdfDocument,
        rect,
        currentPage,
        pageSize,
        totalPage,
      ];

  PdfViewState copyWith({
    File? pdfDocument,
    Rect? rect,
    int? currentPage,
    Size? pageSize,
    int? totalPage,
  }) {
    return PdfViewState(
      pdfDocument: pdfDocument ?? this.pdfDocument,
      rect: rect ?? this.rect,
      currentPage: currentPage ?? this.currentPage,
      pageSize: pageSize ?? this.pageSize,
      totalPage: totalPage ?? this.totalPage,
    );
  }
}
